from __future__ import print_function
from time import clock

class Timer:
    def __init__(self, text):
        self.text = text

    def __enter__(self):
        self.start = clock()
        return self

    def __exit__(self, *args):
        self.end = clock()
        self.interval = self.end - self.start
        print(self.text + ':', self.interval, 's')


def time_real(f):
    def f_timed(*args, **kw):
        ts = clock()
        result = f(*args, **kw)
        te = clock()
        print('%s: %.3f' % (f.__name__, te - ts))
        return result
    return f_timed