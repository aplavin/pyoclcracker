# coding=utf-8

class DisplayMode:
    NoDisplay = 0
    OneLine = 1
    Verbose = 2
    Curses = 3

import sys
import curses
import config
from utils import gen_nth
from classes.buffer import Buffer
import locale
code = locale.setlocale(locale.LC_ALL, '')

def ellipsize(string, max_len):
    if len(string) <= max_len:
        return string
    else:
        max_len -= 3
        at_beginning = (max_len + 1) / 2
        at_end = max_len / 2
        return string[:at_beginning] + '...' + string[-at_end:]

def to_eng(value):
    suffixes = ['', 'K', 'M', 'G', 'T', 'P']
    suf_ind = 0
    while value >= 1000:
        if suf_ind == len(suffixes) - 1: break

        value /= 1000.0
        suf_ind += 1

    return '%.*f %s' % (3 if value < 10 else 2 if value < 100 else 1, value, suffixes[suf_ind])

def sec_to_str(value):
    if value < 0: value = 0
    f_value = value
    value = int(value)
    if 0 < f_value < 1:
        return '%d ms' % (f_value * 1000)
    elif value < 60:
        return '%d s' % value
    elif value < 3600:
        return '%d m %d s' % (value / 60, value % 60)
    elif value < 24 * 3600:
        value /= 60
        return '%d h %d m' % (value / 60, value % 60)
    elif value < 30 * 24 * 3600:
        value /= 3600
        return '%d d %d h' % (value / 24, value % 24)
    elif value < 365 * 24 * 3600:
        value /= 24 * 3600
        return '%d m %d d' % (value / 30, value % 30)
    else:
        value /= 365 * 24 * 3600
        return '%d y' % value

def get_progress_bar(value, max_value, width = 80):
    filled_cnt = int(value * width / max_value)
    return u'█' * filled_cnt + ' ' * (width - filled_cnt - 1)


def display_info(
        max_n, hashes_cnt_all, hashes_cnt_last,
        time_all, time_last,
        hashes_cracked, hashes_saved,
        gpu_utilization, gpu_temperature, throttling):
    pps_last = hashes_cnt_last / time_last
    pps_all = hashes_cnt_all / time_all

    elapsed = time_all
    to_complete = max_n / (pps_all + 1)
    remained = to_complete - elapsed

    if config.display_mode == DisplayMode.NoDisplay:
        return
    elif config.display_mode == DisplayMode.OneLine:
        print '[%sh/s | %sh/s (%.1f%%)] [%s of %s] [%s | %s | %s] [%d]' % (
            to_eng(pps_last), to_eng(pps_all), pps_all * 100.0 / pps_last,
            to_eng(hashes_cnt_all), to_eng(max_n),
            sec_to_str(elapsed), sec_to_str(remained), sec_to_str(to_complete),
            hashes_cracked
        ), '\r',
    elif config.display_mode == DisplayMode.Verbose:
        buf = Buffer(sys.stdout)
        print >> buf, '\n' * 20
        print >> buf, 'Speed: virt %sh/s, real %sh/s (%.1f%%)' % (to_eng(pps_last), to_eng(pps_all), pps_all * 100.0 / pps_last)
        if throttling: print >> buf, '[THROTTLING]'
        print >> buf
        print >> buf, 'Completed: %s of %s' % (to_eng(hashes_cnt_all), to_eng(max_n))
        print >> buf, '%.2f%% [%s]' % (hashes_cnt_all * 100.0 / max_n, get_progress_bar(hashes_cnt_all, max_n, 70))
        print >> buf, 'Processing: "%s"' % gen_nth(hashes_cnt_all, mask=True)
        print >> buf
        print >> buf, 'Elapsed: %s | Remaining: %s | Total: %s' % (sec_to_str(elapsed), sec_to_str(remained), sec_to_str(to_complete))
        print >> buf, 'Cracked passwords: %d, saved %d' % (hashes_cracked, hashes_saved)
        print >> buf
        print >> buf, u'GPU: utilization %d%%, temperature %.1f°C (throttle at %.1f°C)' % (gpu_utilization, gpu_temperature, config.max_temperature)
        buf.flush()
    elif config.display_mode == DisplayMode.Curses:
        scr.erase()

        win = scr.subwin(3, 80, 0, 0)
        win.addstr(1, 1, 'pyOclCracker - LinkedIn SHA1')
        win.addstr(1, 39, "'q' to quit".rjust(40))
        win.border()
        win.noutrefresh()

        width = 47
        win = scr.subwin(6, width + 2, 2, 0)
        win.addstr(1, 1, 'Progress'.center(width))
        win.addstr(2, 1, 'Completed: %s of %s' % (to_eng(hashes_cnt_all), to_eng(max_n)))
        win.addstr(3, 1, '[%s] %.2f%%' % (' ' * (width - 10), hashes_cnt_all * 100.0 / max_n))
        for i, c in enumerate(get_progress_bar(hashes_cnt_all, max_n, width - 10)):
            win.addch(3, 2 + i, ' ' if c == ' ' else curses.ACS_BLOCK)
        win.addstr(4, 1, 'El: %s | Rem: %s | Tot: %s' % (sec_to_str(elapsed), sec_to_str(remained), sec_to_str(to_complete)))
        win.border()
        win.noutrefresh()

        width = 80 - width - 3
        win = scr.subwin(6, width + 2, 2, 80 - width - 2)
        win.addstr(1, 1, 'Speed'.center(width))
        win.addstr(2, 1, 'Virtual: %sh/s' % to_eng(pps_last))
        win.addstr(3, 1, 'Real: %sh/s (%.1f%%)' % (to_eng(pps_all), pps_all * 100.0 / pps_last))
        if throttling:
            win.addstr(4, 1, 'THROTTLING')
        win.border()
        win.noutrefresh()

        win = scr.subwin(5, 80, 7, 0)
        win.addstr(1, 1, 'Alphabet size'.center(78))
        win.addstr(2, 1, 'Pos')
        win.addstr(3, 1, 'Cnt')
        win.vline(2, 5, curses.ACS_VLINE, 2)
        places = 0
        for i, chset in enumerate(config.alphabet):
            cur_places = len(str(chset.num))
            win.addstr(2, 8 + places, ('%d' % (i + 1)).rjust(cur_places))
            win.addstr(3, 8 + places, '%d' % chset.num)
            places += cur_places + 3
            win.vline(2, 6 + places, curses.ACS_VLINE, 2)
        win.border()
        win.noutrefresh()

        width = 39
        win = scr.subwin(6, width + 2, 11, 0)
        win.addstr(1, 1, 'Stats'.center(width))
        win.addstr(2, 1, 'Hashlist size: %d' % config.hashlist_size)
        win.addstr(3, 1, 'Cracked: %d (saved: %d)' % (hashes_cracked, hashes_saved))
        win.addstr(4, 1, 'Processing: %s' % gen_nth(hashes_cnt_all, mask=True))
        win.border()
        win.noutrefresh()

        width = 80 - width - 3
        win = scr.subwin(6, width + 2, 11, 80 - width - 2)
        win.addstr(1, 1, 'GPU Info'.center(width))
        win.addstr(2, 1, u'Temperature: %.1f°C (throttle at %d°C)'.encode(locale.getpreferredencoding()) % (gpu_temperature, config.max_temperature))
        win.addstr(3, 1, 'Utilization: %d%%' % gpu_utilization)
        win.border()
        win.noutrefresh()

        scr.addch(2, 0, curses.ACS_LTEE)
        scr.addch(2, 48, curses.ACS_TTEE)
        scr.addch(2, 79, curses.ACS_RTEE)
        scr.addch(7, 0, curses.ACS_LTEE)
        scr.addch(7, 48, curses.ACS_BTEE)
        scr.addch(7, 79, curses.ACS_RTEE)
        scr.addch(11, 0, curses.ACS_LTEE)
        scr.addch(11, 40, curses.ACS_TTEE)
        scr.addch(11, 79, curses.ACS_RTEE)
        scr.addch(16, 40, curses.ACS_BTEE)
        scr.noutrefresh()

        curses.doupdate()

        if scr.getch() == ord('q'):
            exit()