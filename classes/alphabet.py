import re
from utils import crange

class Alphabet:
    def __init__(self, chars):
        assert len(chars) == len(set(chars))
        self.chars = ''.join(chars)
        self.num = len(chars)

    @classmethod
    def from_human(cls, string):
        if re.match('^.$', string):
            return cls(string[0])
        elif re.match('^.-.$', string) or re.match(r'.\.{2,3}.', string):
            return cls(crange(string[0], string[-1]))
        elif string.startswith('?'):
            dct = {
                'l': crange('a', 'z'),
                'L': crange('A', 'Z'),
                'd': crange('0', '9'),
                's': ' !"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~',
                }
            return cls([c for k in string[1:] for c in dct[k]])
        else:
            return cls(string)

    def __str__(self):
        return '%s (%d)' % (self.chars.encode('string-escape'), self.num)