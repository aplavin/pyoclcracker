class Buffer:
    def __init__(self, file):
        self.file = file
        self.string = ''

    def write(self, string):
        self.string += string

    def flush(self):
        self.file.write(self.string)
        self.file.flush()