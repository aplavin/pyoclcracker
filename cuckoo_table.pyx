#cython: cdivision = True
#cython: boundscheck = False
#cython: wraparound = False

import numpy as np
cimport numpy as np

cdef unsigned h1(unsigned long long hash, unsigned r, unsigned table_size) nogil:
    return (<unsigned>(hash >> 32) ^ r) % table_size

cdef unsigned h2(unsigned long long hash, unsigned r, unsigned table_size) nogil:
    return (<unsigned>hash ^ r) % table_size

def build(
np.ndarray[np.uint64_t, ndim=1] hashes,
np.ndarray[np.uint64_t, ndim=1] table,
unsigned r1, unsigned r2):
    cdef int i, loc, its, loc1, loc2
    cdef unsigned long long  hash

    cdef unsigned hl = hashes.shape[0]
    cdef unsigned tl = table.shape[0]

    cdef int failed = 0

    for i in range(0, hl):
        hash = hashes[i]
        loc = h1(hash, r1, tl)
        for its in range(50):
            table[loc], hash = hash, table[loc]
            if not hash:
                break

            loc1 = h1(hash, r1, tl)
            loc2 = h2(hash, r2, tl)
            if loc == loc1: loc = loc2
            else: loc = loc1

        if hash:
            failed += 1

    return not failed