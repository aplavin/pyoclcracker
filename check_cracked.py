from hashlib import sha1
import numpy as np
import os
import argparse
import shutil
from classes.timer import Timer

def read_hashes():
    data = np.fromfile('hashes.dat', dtype = 'u4,u4,u4,u4,u4')
    d1 = data['f2'].astype(np.uint64) | (data['f1'].astype(np.uint64) << 32)
    d1.sort()
    d2 = data['f4'].astype(np.uint64) | (data['f3'].astype(np.uint64) << 32)
    d2.sort()
    return d1, d2

def check_cracked(hs1, hs2, cracked):
    cracked_hashes = np.array([sha1(pwd).digest() for pwd in cracked], dtype = 'u4,>u8,>u8')
    cr1 = cracked_hashes['f1']
    cr2 = cracked_hashes['f2']

    index1 = hs1.searchsorted(cr1)
    index2 = hs2.searchsorted(cr2)

    # clip values to have no exception when index points after the last element
    np.clip(index1, 0, len(hs1) - 1, out=index1)
    np.clip(index2, 0, len(hs2) - 1, out=index2)

    checked = np.take(cracked, np.nonzero((hs1[index1] == cr1) & (hs2[index2] == cr2))[0] )
    failed = np.take(cracked, np.nonzero( (hs1[index1] != cr1) | (hs2[index2] != cr2) )[0] )
    return checked, failed


with Timer('Read hashes'):
    hs1, hs2 = read_hashes()

with Timer('Total (without read hashes)'):
    parser = argparse.ArgumentParser()
    parser.add_argument('add_file', type=file, nargs='?')
    parser.add_argument('--ok-file', type=file, default='cracked/_all.txt')
    parser.add_argument('--failed-file', type=file, default='cracked/_failed.txt')
    args = parser.parse_args()

    if args.add_file is not None:
        file = args.add_file
        ok_file = args.ok_file
        failed_file = args.failed_file

        print
        ok_lines = set(ok_file.read().splitlines())
        print 'In ok-file "%s":\n%d passwords' % (ok_file.name, len(ok_lines))
        failed_lines = set(failed_file.read().splitlines())
        print 'In failed-file "%s":\n%d passwords' % (failed_file.name, len(failed_lines))
        print

        print 'Adding cracked from "%s": ' % file.name
        cracked = file.read().splitlines()
        checked, failed = check_cracked(hs1, hs2, cracked)
        checked, failed = set(checked), set(failed)
        print 'Total: %d | OK: %d | Failed: %d' % (len(cracked), len(checked), len(failed))
        print

        new_ok_lines = ok_lines.union(checked)
        new_failed_lines = failed_lines.union(failed)
        print 'Now ok-passwords: %d (%d were duplicates)' % (len(new_ok_lines), len(ok_lines) + len(checked) - len(new_ok_lines))
        print 'Now failed-passwords: %d (%d were duplicates)' % (len(new_failed_lines), len(new_failed_lines) - len(failed_lines) - len(failed))
        print

        print 'Saving...'
        shutil.copy2(ok_file.name, '%s.bak' % ok_file.name)
        shutil.copy2(failed_file.name, '%s.bak' % failed_file.name)

        with open(ok_file.name, 'w') as f:
            f.writelines('%s\n' % x for x in new_ok_lines)
        with open(failed_file.name, 'w') as f:
            f.writelines('%s\n' % x for x in new_failed_lines)

        print 'Sorting files...'
        os.system('sort "%s" -o "%s"' % ((ok_file.name, ) * 2))
        os.system('sort "%s" -o "%s"' % ((failed_file.name, ) * 2))
        print
    else:
        print('-' * 80)
        all_checked, all_failed = set(), set()

        for fname in sorted(os.listdir('cracked')):
            print ('[%s]:' % fname).ljust(20),

            cracked = open(os.path.join('cracked', fname)).read().splitlines()
            checked, failed = check_cracked(hs1, hs2, cracked)

            all_checked.update(checked)
            all_failed.update(failed)

            if not any(failed):
                print 'OK %d' % len(cracked)
            else:
                print 'FAILED %d of %d' % (len(failed), len(cracked))

        print '-' * 80

        print '[Total (unique)]:'.ljust(20),
        if not all_failed:
            print 'OK %d' % len(all_checked)
        else:
            print 'FAILED %d of %d' % (len(all_failed), len(all_checked))