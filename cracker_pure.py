import config
import numpy as np
import struct
from hashlib import sha1
from utils import gen_nth

def prepare():
    global d1, d2
    data = np.fromfile('hashes.dat', dtype = [('f0', np.uint32), ('f1', np.uint32), ('f2', np.uint32), ('f3', np.uint32), ('f4', np.uint32)])
    d1 = set(data['f2'].astype(np.uint64) | (data['f1'].astype(np.uint64) << 32))
    d2 = set(data['f4'].astype(np.uint64) | (data['f3'].astype(np.uint64) << 32))

def brute():
    for n in xrange(config.keyspace_size):
        s = gen_nth(n)
        digest = sha1(s).digest()
        p1, = struct.unpack_from('>Q', digest, 4)
        p2, = struct.unpack_from('>Q', digest, 12)
        p1 = np.uint64(p1)
        p2 = np.uint64(p2)
        if p1 in d1 and p2 in d2:
            yield n