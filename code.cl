#define A0 0x67452301
#define B0 0xEFCDAB89
#define C0 0x98BADCFE
#define D0 0x10325476
#define E0 0xC3D2E1F0


#define ROTL(x, n)  ( ( (x) << (n) ) | ( (x) >> ( 32 - (n) ) ) )
#define REVERSE(value) value = ROTL( ((value & 0xFF00FF00) >> 8) | ((value & 0x00FF00FF) << 8) , 16)

ulong sha1(uint16 W, const int length)
{
	uint A = A0;
	uint B = B0;
	uint C = C0;
	uint D = D0;
	uint E = E0;

% for i in range((len(alphabet) + 1 + 3) / 4):
    REVERSE(W.s${i});
% endfor

<% def hex(x): return chr( (ord('0') + x) if x < 10 else (ord('A') + x - 10) ) %>

<%def name="F(n, b, c, d)" filter="trim">
    % if 0 <= n < 20:
    ( ${d} ^ ( ${b} & ( ${c} ^ ${d} ) ) )
    % elif 20 <= n < 40:
    ( ${b} ^ ${c} ^ ${d} )
    % elif 40 <= n < 60:
    ( ( ${b} & ${c} ) | ( ${d} & ( ${b} | ${c} ) ) )
    % elif 60 <= n < 80:
    ( ${b} ^ ${c} ^ ${d} )
    % endif
</%def>

<%def name="K(n)" filter="trim">
    % if 0 <= n < 20:
    0x5A827999
    % elif 20 <= n < 40:
    0x6ED9EBA1
    % elif 40 <= n < 60:
    0x8F1BBCDC
    % elif 60 <= n < 80:
    0xCA62C1D6
    % endif
</%def>

<%def name="expand(n)" filter="trim"> \
    % if n < 16:
    W.s${hex(n)}
    % else:
    (W.s${hex(n & 15)} = ROTL(W.s${hex(n & 15)} ^ W.s${hex((n - 14) & 15)} ^ W.s${hex((n - 8) & 15)} ^ W.s${hex((n - 3) & 15)}, 1) )
    % endif
</%def>

<% vars_list = ['A', 'B', 'C', 'D', 'E'] %>
<%def name="sub_round(n)" filter="trim">
    <% vars = vars_list[-(n % 5):] + vars_list[:-(n % 5)] %>
    ${vars[-1]} += ROTL(${vars[0]}, 5) + ${F(n, *vars[1:4])} + ${K(n)} + ${expand(n)};
    ${vars[1]} = ROTL(${vars[1]}, 30);
</%def>

% for i in range(80):

    // round #${i / 20}, subround #${i % 20} (total #${i})
    ${sub_round(i)}
% endfor

//	A += a0;
//	B += b0;
//	C += c0;
//	D += d0;
//	E += e0;

    return as_ulong((uint2)(D, E));
//    return ((ulong)E << 32) | D;
//    return upsample(E, D);
}

#define h1(k) ( (uint)( (uint)((k) >> 32) ^ ${rand_0}) % ${table_size} )
#define h2(k) ( (uint)( (uint)(k) ^ ${rand_1}) % ${table_size} )

bool is_in_table(
	__global const ulong *table,
	const ulong hash)
{
    return table[h1(hash)] == hash | table[h2(hash)] == hash;
//	return table[h1(hash)] == hash | (table[h1(hash)] != 0 && table[h2(hash)] == hash);
}

__constant char alphabet[${len(alphabet)}][${max(cpos.num for cpos in alphabet)}] = {
    {${'},\n    {'.join(', '.join('%d' % ord(c) for c in cpos.chars) for cpos in alphabet)}}
};

__attribute__((reqd_work_group_size(${workgroup_size}, 1, 1)))
__kernel void do_brute(
	__global const ulong *table,
	const ulong start_num,
	__global ulong *result,
	__global uint *res_ind)
{
    __local char _alphabet[${len(alphabet)}][128];

    int lid = get_local_id(0);
    if (lid < 128)
    {
    % for i in range(len(alphabet)):
        _alphabet[${i}][lid] = alphabet[${i}][lid];
    % endfor
    }
    barrier(CLK_LOCAL_MEM_FENCE);


	uint16 s_u16, s_u16_t;
	s_u16 = 0;
	char *s = (char*)&s_u16;

	int i;
	
	ulong start = start_num + get_global_id(0);
	ulong n = start;

% for i in range(0, len(alphabet) - 1):
    s[${i}] = _alphabet[${i}][n % ${alphabet[i].num}];
    n /= ${alphabet[i].num};
% endfor
    s[${len(alphabet) - 1}] = _alphabet[${len(alphabet) - 1}][0];

    s[${len(alphabet)}] = 0x80;
    s_u16.sF = ${len(alphabet)} * 8;

//	#pragma unroll 1
	for (i = 0; i < ${alphabet[-1].num}; i++)
	{
	    s_u16_t = s_u16;

		if (is_in_table(table, sha1(s_u16_t, ${len(alphabet)})))
		{
			result[atomic_inc(res_ind)] = start * ${alphabet[-1].num} + i + 1;
		}
	    s[${len(alphabet) - 1}] = _alphabet[${len(alphabet) - 1}][i + 1];
	}
}


