import argparse
import operator
from classes.timer import Timer
from os.path import join, isfile, isdir, basename, dirname
from itertools import product
import math
import pyximport; pyximport.install()
import dict_stats_c
from classes.display import to_eng, sec_to_str

def scan(lst):
    res = list(lst)
    for i in range(len(res) - 1):
        res[i + 1] += res[i]
    return res

def all_same(items):
    it = iter(items)
    first = next(it, None)
    return all(x == first for x in it)

def calc_alphabet_lengths(counters, keyspace):
    scan_cntr = lambda cntr: scan( sorted([cnt for val, cnt in cntr.items()], reverse=True) )
    scans = [scan_cntr(cntr) for cntr in counters]
    assert all_same(scn[-1] for scn in scans)
    lengths = [1] * len(counters)

    calc_ks = lambda: reduce(operator.mul, lengths)
    calc_cnts = lambda: [scn[leng - 1] for scn, leng in zip(scans, lengths)]

    while calc_ks() < keyspace:
        cnts = calc_cnts()
        min_ind = min(range(len(cnts)), key=lambda i: cnts[i])
        lengths[min_ind] += 1

    return lengths, min(calc_cnts()) * 1.0 / scans[0][-1], calc_ks()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('dict_file', type=file)
    parser.add_argument('--output-dir', '-od')
    parser.add_argument('--len', '-l', type=int, default=6)
    parser.add_argument('--speed', '-spd', type=int, default=150)
    args = parser.parse_args()

    output_dir = args.output_dir if args.output_dir is not None else join(dirname(args.dict_file.name), 'stats/')
    stats_fname = join(output_dir, '[charsets] ' + basename(args.dict_file.name))

    counters = dict_stats_c.process_dict(args.dict_file, length=args.len)

    with open(stats_fname, 'w') as f:
        print >> f, 'Length = %d' % args.len
        print >> f, '-' * 80
        print >> f
        if not any(counters):
            print >> f, '[No words]'
        else:
            max_keyspace = reduce(operator.mul, [len(cntr) for cntr in counters])

            for keyspace in [10**(x/2.0) for x in range(12, int(math.ceil(2 * math.log10(max_keyspace))) + 1)]:
                if keyspace > max_keyspace: keyspace = max_keyspace

                print >> f

                lengths, coverage, keyspace = calc_alphabet_lengths(counters, keyspace)
                print >> f, 'Keyspace: %s (%s at %d M/s)' % (to_eng(keyspace), sec_to_str(keyspace / args.speed / 10**6), args.speed)
                print >> f, 'Approximate coverage of dictionary: %.3f%%' % (coverage * 100)
                print >> f, 'Charset:'

                print >> f, '-' * 80
                for counter, length in zip(counters, lengths):
                    counter = [(counter[chr(val)], val) for val in range(256) if counter[chr(val)]]
                    counter.sort(reverse=True)
                    counter = counter[:length]
                    print >> f, ''.join(chr(val) for cnt, val in counter)
                print >> f, '-' * 80
        print >> f

if __name__ == '__main__':
    with Timer('Total'):
        main()