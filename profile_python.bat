@echo off
del /Q cprofile.pstats
del /Q profile.png
python -m cProfile -o cprofile.pstats %1 %2
gprof2dot.py -n0.05 -e0 -f pstats cprofile.pstats | dot -Tpng -o profile.png
profile.png