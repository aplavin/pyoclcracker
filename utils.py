import config
import numpy as np

def crange(start, stop):
    return [chr(c) for c in range(ord(start), ord(stop) + 1)]

def gen_nth(n, mask = False):
    s = ''
    last_char = config.alphabet[-1].chars[n % config.alphabet[-1].num]
    n /= config.alphabet[-1].num
    for i in xrange(len(config.alphabet) - 1):
        s += config.alphabet[i].chars[n % config.alphabet[i].num]
        n /= config.alphabet[i].num
    s += last_char if not mask else '*'
    return s

def convert_hashes():
    hash_strs = np.fromfile('combo_not.txt', 'a42') # 42 because of CRLF
    hashes = np.fromiter(((np.uint32(int(s[0:8], 16)) & 0x00000FFF, np.uint32(int(s[8:16], 16)), np.uint32(int(s[16:24], 16)), np.uint32(int(s[24:32], 16)), np.uint32(int(s[32:40], 16))) for s in hash_strs), dtype='I,I,I,I,I', count=len(hash_strs))
    hashes = np.unique(hashes)
    hashes.tofile('hashes.dat')

def read_hashes():
    hashes = np.rec.fromfile('hashes.dat', formats='I,I,I,I,I')
    config.hashlist_size = len(hashes)
    hashes['f1'] -= 0xEFCDAB89
    hashes['f2'] -= 0x98BADCFE
    hashes['f3'] -= 0x10325476
    hashes['f4'] -= 0xC3D2E1F0
    rotr = lambda x, n: (x >> n) | (x << (32 - n))
    #    hashes['f3'] = rotr(hashes['f3'], 30)
    #    hashes['f4'] = rotr(hashes['f4'], 30)
    xors = (hashes['f3'].astype(np.uint64) | (hashes['f4'].astype(np.uint64) << 32))

    #    print len(xors), len(np.unique(xors))
    return np.unique(xors)