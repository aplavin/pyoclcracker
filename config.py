import operator
from pyopencl import device_type
import adl3
from classes.alphabet import Alphabet
from classes.display import DisplayMode
from classes.mode import Mode

# brute parameters

mode = Mode.BruteAlphabet
display_mode = DisplayMode.Curses
cracked_file = 'cracked.txt'
#with open(cracked_file, 'w'): pass # uncomment if you want to clear the file at the beginning

alphabet_str = r'''
?L
?l
?l
?l
?l
?l
?l
?l
'''

# program setting
alphabet = [Alphabet.from_human(part.decode('string-escape')) for part in alphabet_str.splitlines() if len(part) > 0]
keyspace_size = reduce(operator.mul, (charpos.num for charpos in alphabet))
hashes_per_workitem = alphabet[-1].num
dev_type = device_type.GPU
dev_index = 0
adl_device = adl3.get_devices()[0]
max_temperature = 90
workgroup_size = 256
table_len = 2 ** 24
kernel_time_desired = 0.02
print_interval = 2
save_passwords_interval = 30
randoms = [3296617091, 3443809610]