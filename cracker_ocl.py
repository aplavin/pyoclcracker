from time import sleep
import pyopencl as cl
from pyopencl import mem_flags as mf
import numpy as np
from classes.display import display_info
from utils import *
from itertools import count
import config
import adl3
from mako.template import Template
import thread
import pyximport; pyximport.install(setup_args = {'include_dirs': [np.get_include()]})
import cuckoo_table

def _prepare_ocl():
    global ctx, queue, prg

    ctx = cl.Context(devices = [ cl.get_platforms()[0].get_devices(config.dev_type)[config.dev_index] ])
    queue = cl.CommandQueue(ctx, properties = cl.command_queue_properties.PROFILING_ENABLE)
    adl3.init()

    params = {
        'num_hashes': config.hashes,
        'table_size': config.table_len,
        'alphabet': config.alphabet,
        'workgroup_size': config.workgroup_size
    }
    params.update(('rand_%d' % i, r) for i, r in enumerate(config.randoms))

    with open('code_rendered.cl', 'w') as f:
        rendered = Template(file('code.cl', 'r').read(), disable_unicode=True, strict_undefined=True).render(**params)
        f.write(rendered)
    prg = cl.Program(ctx, file('code_rendered.cl', 'r').read()).build(options='-w')

def _build_table():
    global table_buf
    table = np.zeros(config.table_len, dtype = np.uint64)
    success = cuckoo_table.build(config.hashes, table, config.randoms[0], config.randoms[1])
    table_buf = cl.Buffer(ctx, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf = table)
    return success

def _save_passwords(cracked):
    with open(config.cracked_file, 'a') as f:
        f.writelines('%s\n' % gen_nth(int(n)) for n in cracked)

def brute():
    cracked_saved_cnt, cracked_cnt = 0, 0
    cracked = np.empty(2**20, np.uint64)

    result = np.zeros(2 ** 20, np.uint64)
    result_buf = cl.Buffer(ctx, mf.WRITE_ONLY | mf.COPY_HOST_PTR, hostbuf = result)
    res_cnt = np.zeros(1, dtype=np.uint32)
    res_cnt_buf = cl.Buffer(ctx, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf = res_cnt)

    workgroups_per_run = 1
    start_index, hashes_cnt_all = 0, 0
    evt_first = None
    last_printed, last_copied = -100,-100
    for i in count():
        if hashes_cnt_all >= config.keyspace_size:
            break

        evt = prg.do_brute(queue,
            (config.workgroup_size * workgroups_per_run,), (config.workgroup_size,),
            table_buf,
            np.uint64(start_index),
            result_buf, res_cnt_buf
        )
        if i == 0: evt_first = evt

        hashes_cnt_last = config.hashes_per_workitem * config.workgroup_size * workgroups_per_run
        hashes_cnt_all += hashes_cnt_last
        start_index += config.workgroup_size * workgroups_per_run

        (lambda: evt.wait())()

        time_last = (evt.profile.end - evt.profile.start) / 1000.0 ** 3
        time_all = (evt.profile.end - evt_first.profile.start) / 1000.0 ** 3

        if time_all - last_printed > config.print_interval:
            last_printed = time_all

            config.adl_device.update_activity()
            throttling = False
            if config.adl_device.temperature > config.max_temperature:
                throttling = True

            cl.enqueue_copy(queue, res_cnt, res_cnt_buf)
            cracked_cnt = cracked_saved_cnt + res_cnt[0]

            display_info(
                config.keyspace_size, hashes_cnt_all, hashes_cnt_last,
                time_all, time_last,
                cracked_cnt, cracked_saved_cnt,
                config.adl_device.utilization, config.adl_device.temperature, throttling)

            if throttling: sleep(2)

        if time_all - last_copied > config.save_passwords_interval:
            last_copied = time_all

            cl.enqueue_copy(queue, result, result_buf)

            _result = np.unique(result[(0 < result) & (result <= config.keyspace_size)]) - 1
            thread.start_new_thread(_save_passwords, (_result,))
            cracked[cracked_saved_cnt : cracked_saved_cnt + len(_result)] = _result
            cracked_saved_cnt += len(_result)

            result = np.zeros_like(result)
            cl.enqueue_copy(queue, result_buf, result)
            res_cnt = np.zeros_like(res_cnt)
            cl.enqueue_copy(queue, res_cnt_buf, res_cnt)

        # clipping so that not to change very fast in case of fluctuations, +1 to guarantee that it will not be 0
        workgroups_per_run = int(workgroups_per_run * np.clip(config.kernel_time_desired / time_last, 0.5, 2)) + 1


    cl.enqueue_copy(queue, result, result_buf)
    _result = np.unique(result[(0 < result) & (result <= config.keyspace_size)]) - 1
    _save_passwords(_result)
    cracked[cracked_saved_cnt : cracked_saved_cnt + len(_result)] = _result
    cracked_saved_cnt += len(_result)

    display_info(
        config.keyspace_size, config.keyspace_size, hashes_cnt_last,
        time_all, time_last,
        cracked_saved_cnt, cracked_saved_cnt,
        config.adl_device.utilization, config.adl_device.temperature, False)

    return np.sort(cracked[:cracked_saved_cnt])

def prepare():
    _prepare_ocl()
    built = _build_table()
    assert built