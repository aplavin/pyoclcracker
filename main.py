import numpy as np
from classes.display import DisplayMode
from classes.mode import Mode
from classes.timer import Timer
import config
import cracker_ocl, cracker_pure
from utils import read_hashes

def test_correctness():
    config.display_info = 1
    print 'Testing correctness...'
    print 'Alphabet: %s' % '|'.join(str(x) for x in config.alphabet)
    print 'Keyspace size: %d' % config.keyspace_size
    # compute etalon list of cracked using pure python simple cracker
    cracker_pure.prepare()
    etalon = list(cracker_pure.brute())
    print 'Etalon length: %d' % len(etalon)

    # compute actual list using ocl cracker
    cracker_ocl.prepare()
    for_test = cracker_ocl.brute()
    print 'Testing length: %d' % len(for_test)

    print '=== %s ===' % ('Correct!' if np.array_equal(etalon, for_test) else 'Wrong!')

def main(scr):
    if config.display_mode == DisplayMode.Curses:
        curses.curs_set(0)
        scr.nodelay(True)
        import classes.display
        classes.display.scr = scr

    config.hashes = read_hashes()

    if config.mode == Mode.TestCorrectness:
        test_correctness()
    elif config.mode == Mode.BruteAlphabet:
        cracker_ocl.prepare()
        cracker_ocl.brute()

    if config.display_mode == DisplayMode.Curses:
        scr.nodelay(False)
        scr.getch()

if __name__ == '__main__':
    with Timer('Total'):
        if config.display_mode == DisplayMode.Curses:
            import curses
            curses.wrapper(main)
        else:
            main(None)

    import os
    os.system('sort -u "%s" -o "%s"' % (config.cracked_file, config.cracked_file))