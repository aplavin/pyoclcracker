#cython: cdivision = True
#cython: boundscheck = False
#cython: wraparound = False
from collections import Counter

def process_dict(file, int length):
    cdef int i, j, word_len
    cdef bytes word
    cdef char* c_word
    cdef list counters = [[0 for j in range(256)] for i in range(length)]

    for word in file:
        word_len = len(word) - 1 # because of endline
        if word_len == length:
            c_word = word
            for i in range(word_len):
                counters[i][c_word[i]] += 1

    counters = [ Counter({chr(val): cntr[val] for val in range(256) if cntr[val]}) for cntr in counters]
    return counters